rem <<< For this program to work the directory C:\PDFCompression\ must exist and the program 4dotsFreePDFCompress.exe 
rem <<< must exist inside of it. eg C:\PDFCompression\4dotsFreePDFCompress.exe 

rem <<< First we set the variables we are goana use 
rem <<< The variable "current_date" wil hold the current date for use later.
rem <<< The variable "source_directory" points to where the program should search for PDF's to compress.

	set vatiable-name=current_date
	set variable-name=source_directory

rem <<< Then we set the value of those variables mentioned above
rem <<< Here you can set the valus of "source_directory" 
rem <<< %date% returns the current date

	set source_directory=J:\2018
	set current_date=%date:* =%

rem <<< We then search the %source_directory% for all *.pdf files that were created on the %current_date% 
rem <<< We then export that list to C:\PDFCompression\unformattedlist.txt 

	forfiles /P %source_directory% /S /M *.pdf /D +%current_date% /C "cmd /c echo @path ">C:\PDFCompression\unformattedlist.txt

rem <<< Unfortunetally the output is in a format that contains a list of quoted file paths and the next step 
rem <<< requires that the list only comma sepperated without any quotations 
rem <<< so the next step removes all quotation marks from the file and outputs the result to C:\PDFCompression\formattedlist.txt 

	@echo off
	setlocal EnableDelayedExpansion
	(
	for /f "delims=" %%A in (C:\PDFCompression\unformattedlist.txt) do (
	
	   set a=%%A
	   set a=!a:"=!
	   echo(!a!
	)
	)>C:\PDFCompression\formattedlist.txt

rem <<< We then pass the propperly formatted list to 4dotsFreePDFCompress.exe which accepts a list of files as an instruction to compress them.
rem <<< We also log the process at C:\PDFCompression\log.txt the log file will be ammended not overwritten 

	C:\PDFCompression\4dotsFreePDFCompress.exe /l:"C:\PDFCompression\formattedlist.txt" /overwrite /quality:10 /log:"C:\PDFCompression\log.txt"

pause
